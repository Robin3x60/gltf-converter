var scene, camera, renderer, orbitcontrols;
var pointLight, ambientLight;
var legacyJsonLoader, objectLoader, gltfExporter;
var files = fileList;
var inputIndex, last, next, download, exportAll, webGL;
var index = -1;
var currentModel, currentShowModel, currentPath
var interval;

var init = ()=> {

    // Canvas
    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(40, 1200 / 800, .1, 3000);
    camera.position.set(0, 30, 30);
    camera.lookAt(0, 0, 0)

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(1200, 800);
    renderer.setClearColor(0xf9f9ca)
    renderer.gammaFactor = 2.2;
    renderer.gammaOutput = true;

    orbitcontrols = new THREE.OrbitControls(camera, renderer.domElement)

    // Light
    ambientLight = new THREE.AmbientLight(0xffffff, 1)
    ambientLight.name = 'light'
    scene.add(ambientLight)

    // pointLight = new THREE.PointLight(0xffffff, .5)
    // pointLight.position.set(0, 200, 200)
    // pointLight.name = 'light'
    // scene.add(pointLight)

    var directional = new THREE.DirectionalLight(0xFFFFFF, .5)
    directional.name = 'light'
    directional.position.set(0, 200, -200)
    scene.add(directional)

    currentShowModel = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), new THREE.MeshNormalMaterial())
    scene.add(currentShowModel)

    // Loaders
    legacyJsonLoader = new THREE.LegacyJSONLoader();
    objectLoader = new THREE.ObjectLoader();
    gltfExporter = new THREE.GLTFExporter();

    // Buttons
    webGL = document.querySelector('#web-GL');
    webGL.appendChild(renderer.domElement);

    inputIndex = document.querySelector('#index')
    inputIndex.addEventListener('change', (e)=> {

        e.preventDefault()

            let val = e.target.value

            loadModel(parseInt(val))

    })
    inputIndex.maxlength = files.length

    next = document.querySelector('#next');
    next.addEventListener('click', ()=> {

        loadModel(index + 1)

        inputIndex.value = index

    }, false);

    last = document.querySelector('#last');
    last.addEventListener('click', ()=> {

        loadModel(index - 1)

        inputIndex.value = index

    }, false);

    download = document.querySelector('#download');
    download.addEventListener('click', exportGLTF, false);
    
    exportAll = document.querySelector('#exportAll');
    exportAll.addEventListener('click', exportAllModels, false);
}

var loadModel = (i, callback)=> {

    index = i;

    if(files.length > index && index >= 0) {

        // Remove old models
        currentModel = null
        currentPath = null
        scene.remove(currentShowModel)

        scene.children.forEach(child => {

            if(child.name != 'light')
                scene.remove(child)
        })

        var path = window.location.origin+window.location.pathname + '/models/' + files[index]

        // Load new model
        objectLoader.load(path, (model)=> {

            currentModel = model

            currentPath = files[index]
            console.log(currentPath, currentModel)

            model.name = currentPath

            currentShowModel = model//.clone()

            currentShowModel = setModel(currentShowModel)

            scene.add(currentShowModel)
            
            if(callback != null)
                callback()

        }, undefined, (error)=> {
 
            console.log(error.message)
        });

    }
    else {
        index = -1
        alert('No files remaining');
    }

    // Set showcase model 
    function setModel(obj) {

        // if(obj instanceof THREE.Scene) {

        //     obj.children.forEach((child)=> {

        //         if(child instanceof THREE.Mesh) {

        //             let tmp = child['material']

        //             if(tmp.map) {
        //                 var name = tmp.map.image.currentSrc
        //                 var textureLoader = new THREE.TextureLoader()
        //                 var texture = textureLoader.load(name)

        //                 texture.wrapS = THREE.RepeatWrapping
        //                 texture.wrapT = THREE.RepeatWrapping
        //                 texture.repeat.set( 1, 1 )

        //                 texture.encoding = THREE.sRGBEncoding

        //                 texture.anisotropy = 16

        //                 child['material'] = new THREE.MeshPhongMaterial({
        //                     flatShading: true,
        //                     side: THREE.DoubleSide,
        //                     transparent: true,
        //                     color: 0xFFFFFF,
        //                     map: texture
        //                 })
        //             }
        //             else {

        //                 if(child instanceof THREE.Group || child instanceof THREE.Scene) {

        //                     child.children.forEach(child => {

        //                         if(child instanceof THREE.Mesh) {

        //                             let tmp = child['material']
                
        //                             if(tmp.map) {
        //                                 var name = tmp.map.image.currentSrc
        //                                 var textureLoader = new THREE.TextureLoader()
        //                                 var texture = textureLoader.load(name)
                
        //                                 texture.wrapS = THREE.RepeatWrapping
        //                                 texture.wrapT = THREE.RepeatWrapping
        //                                 texture.repeat.set( 1, 1 )
                
        //                                 texture.encoding = THREE.sRGBEncoding
                
        //                                 texture.anisotropy = 16
                
        //                                 child['material'] = new THREE.MeshPhongMaterial({
        //                                     flatShading: true,
        //                                     side: THREE.DoubleSide,
        //                                     transparent: true,
        //                                     color: 0xFFFFFF,
        //                                     map: texture
        //                                 })
        //                             }
        //                         }
        //                     })
        //                 }
        //                 else {

        //                     child['material'] = new THREE.MeshPhongMaterial({
        //                         flatShading: true,
        //                         side: THREE.DoubleSide,
        //                         transparent: true,
        //                         color: 0xFFFFFF
        //                     })
        //                 }
        //             }
        //         }
        //         else if(obj instanceof THREE.Object3D) {

        //             obj.traverse(child => {

        //                 if(child instanceof THREE.Mesh) {

        //                     let tmp = child['material']

        //                     child['material'] = new THREE.MeshPhongMaterial({
        //                         flatShading: true,
        //                         side: THREE.DoubleSide,
        //                         transparent: true,
        //                         color: 0xFFFFFF
        //                     })
        //                 }
        //             })
        //         }
        //     })
        // }


        // obj.traverse(child => {

        //     if(child instanceof THREE.Mesh) {

        //         if(child['material'] != null) {

        //             let mat = child['material']

        //             console.log(mat)
                    
        //             if(mat.map) {

        //                 var name = mat.map.image.currentSrc
        //                 var textureLoader = new THREE.TextureLoader()
        //                 var texture = textureLoader.load(name)

        //                 texture.wrapS = THREE.RepeatWrapping
        //                 texture.wrapT = THREE.RepeatWrapping
        //                 texture.repeat.set( 1, 1 )

        //                 texture.encoding = THREE.sRGBEncoding

        //                 texture.anisotropy = 16

        //                 // child['material'] = new THREE.MeshStandardMaterial({
        //                 //     flatShading: true,
        //                 //     roughness: 1,
        //                 //     metalness: 0,
        //                 //     side: THREE.DoubleSide,
        //                 //     transparent: true,
        //                 //     color: mat.color,
        //                 //     map: texture
        //                 // })
        //                 child['material']['flatShading'] = true
        //                 child['material']['side'] = THREE.DoubleSide
        //                 child['material']['transparent'] = true
        //                 child['material']['color'] = new THREE.Color(0xFFFFFF)
        //                 child['material']['texture'] = texture
        //             }
        //             else {

        //                 // child['material'] = new THREE.MeshStandardMaterial({
        //                 //     flatShading: true,
        //                 //     roughness: 1,
        //                 //     metalness: 0,
        //                 //     side: THREE.DoubleSide,
        //                 //     transparent: true,
        //                 //     // color: new THREE.Color(0xFFFFFF),
        //                 // })
        //                 child['material']['flatShading'] = true
        //                 child['material']['side'] = THREE.DoubleSide
        //                 child['material']['transparent'] = true
        //                 child['material']['color'] = new THREE.Color(0xFFFFFF)
        //             }
        //         }
        //         else {

        //             child['material'] = new THREE.MeshBasicMaterial({
        //                 lights: true,
        //                 // color: new THREE.Color(0xFFFFFF)
        //             })
        //         }
        //     }
        // })

        return obj
    }
}


var exportGLTF = ()=> {

    console.log('Downloading model as GLTF 2.0 ---------------------------------------------------')
    console.log(currentPath)

    var options = {
        truncateDrawRange: false,
        binary: document.getElementById( 'binary' ).checked,
        embedImages: true,
        forceIndices: true,
        forcePowerOfTwoTextures: true
    };
    
    gltfExporter.parse( currentModel, (result)=> {

        let splitted = currentPath.split('.')
        let outPath = splitted[0] + '.gltf'

        var output = JSON.stringify( result, null, 2 );
        var blob = new Blob( [ output ], { type: 'text/plain' } );

        var link = document.createElement('a')
        link.style.display = 'none';
        document.body.appendChild(link)

        link.href = URL.createObjectURL( blob );
        link.download = outPath;
        link.click();

    }, options);
}


function exportAllModels() {

    let i = 0

    console.log('EXPORTING ALL MODELS ------------------------------')

    interval = window.setInterval(()=> {

        loadModel(i, ()=> {

            window.setTimeout(()=> {
                
                exportGLTF()

                i += 1;

                if(i == files.length - 1)
                    clearInterval(interval)

            }, 0)
        })
    }, 3000)
}


var animate = ()=> {

    requestAnimationFrame(animate);

    renderer.render(scene, camera);
};

init();
animate();